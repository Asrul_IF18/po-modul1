import java.awt.Color;
public class Zoo {
	public static void main(String  args []){
		Lion singa = new Lion();
		singa.setNama("Singa");
		singa.setUsia(5);
		singa.setTinggi(100);
		singa.setBerat(100);
		singa.cetakInformasi();

		Horse kuda = new Horse();
		kuda.setNama("Kuda");
		kuda.setUsia(11);
		kuda.setTinggi(160);
		kuda.setBerat(200);
		kuda.cetakInformasi();
		
		Kangoroo kanguru = new Kangoroo();
		kanguru.setNama("Kanguru");
		kanguru.setUsia(7);
		kanguru.setTinggi(120);
		kanguru.setBerat(70);
		kanguru.cetakInformasi();
	}
}
